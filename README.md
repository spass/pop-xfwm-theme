# Pop Xfwm theme

Simple Xfwm theme for Xfce that matches Pop GTK theme from Pop!_OS by System76.

### Instructions
Place all folders in `~/.local/share/themes/` or in `/usr/share/themes/` (for all users)

### Screenshots
Normal/Dark variant:

![pop-xfwm-theme](https://gitlab.com/spass/pop-xfwm-theme/raw/master/screenshot.png)

Light variant:

![pop-xfwm-theme](https://gitlab.com/spass/pop-xfwm-theme/raw/master/screenshot-light.png)

### Link to OpenDesktop.org
https://www.opendesktop.org/p/1299758/